function varargout = select_pow2_pts( datain )
%SELECT_POW2_PTS Select from the input a number of points which is the
%   power of 2 closest to half the length of the time vector
    arguments ( Repeating )
        datain (:,:) double
    end
    
    % find the power of 2 closest to half the length of the time vector
    L2 = length( datain{1} );   % total length
    n = round( log2( L2/2 ) );  % exponent of the closest pow. of 2 to L2/2
    L1 = L2 - 2^n + 1;          % index of the new starting point.
    
    varargout = cell( size( datain) );
    for i=1:length(datain)
        varargout{i} = datain{i}(:, L1:L2);
    end
end