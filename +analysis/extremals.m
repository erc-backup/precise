function output = extremals( data, tolerance )
%EXTREMALS Returns the local maxima and minima. Values closer than a given
%   tolerance are considered as a single value.
    arguments
        data
        tolerance
    end
    
    N = size( data, 1);
    idmax = islocalmax( data, 2 );
    idmin = islocalmin( data, 2 );
    
    id = logical(idmin + idmax);
    
    output = cell(N,1);
    for j = 1:N
        row = data( j, id(j,:) );
        if isempty( row )
            output(j,1) = {data(j,end)};
        else
            output(j,1) = {uniquetol( row, tolerance )};
        end
    end
end

