function [out_data, out_src, j2ind, ind2j] = reduce_dim( data, sources, ...
        dim, direction )
    arguments
        data (:,:) logical
        sources table
        dim (1,1) { mustBeInteger, mustBePositive }
        direction { mustBeMember( direction, ["first", "last"] ) } = 'first'
    end
    
    [C, ~] = size( data );
    dims = cellfun( @length, sources.value )';
    
    % reshape data
    data = reshape( data, [C, dims] );
    % permute dimensions
    data = permute( data, unique( [1+dim, 1:1+length(dims) ], 'stable' ) );
    % reshape data
    data = reshape( data, dims(dim), C, [] );
    
    out_data = nan( size(data, [2,3] ) );
    for c = 1:C
        for j = 1:size(data, 3)
            try
                id = find( data(:,c,j), 1, direction );
                out_data(c,j) = sources.value{ dim }( id );
            catch
            end
        end
    end
    
    function indexes = indexing(j)
        %INDEXING Returns the cell array containing the unique sub-indeces
        %combination corresponding to the index j. Assumes a rectangular
        %n-dimensional matrix of dimensions given by the nonlocal variable
        %'sizes'.
        indexes  = cell( 1, length(dims) );
        [indexes{:}] = ind2sub( dims, j );
        indexes( dim ) = [];
    end
    new_dims = dims;
    new_dims( dim ) = [];
    function J = deindexing( I )
        I(dim) = [];
        J = sub2ind( new_dims, I{:} );
    end
    if nargout > 1
        out_src = sources;
        out_src( dim, :) = [];
        ids = out_src.id > dim;
        out_src.id( ids ) = out_src.id( ids ) - 1;
        j2ind = @indexing;
        ind2j = @deindexing;
    end
end