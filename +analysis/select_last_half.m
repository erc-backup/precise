function [ new_time, varargout ] = select_last_half( time, datain )
    arguments
        time (1,:) double
    end
    arguments ( Repeating )
        datain (:,:) double
    end
    
    if any( size( time, 2) ~= cellfun( @(d) size(d,2), datain ) )
        error( 'Size mismatch' );
    end
    
    % find the second half in the time domain.
    t2 = time >= mean( time( [1, end] ) );
    
    new_time = time( t2 );
    varargout = cell( size( datain) );
    for i=1:length(datain)
        varargout{i} = datain{i}(:, t2 );
    end
end