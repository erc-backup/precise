function [line, Inputs, Outputs, Tags ] = wguide( ax, x, y, i, color, L, ...
    args )
%WGUIDE Summary of this function goes here
%   Detailed explanation goes here
    arguments
        ax
        x (1,1) double
        y (1,1) double
        i (1,1) double { mustBeInteger }
        color (1,:) = 'k'
        L (1,1) double { mustBePositive } = 0.5
        args.Width (1,1) double { mustBePositive } = 5
        args.Direction { mustBeMember( args.Direction, ...
            ["left", "right", "up", "down" ] ) } = "right"
    end

    switch args.Direction
        case "right"
            x = x + [0, +L];
            y = [y y];
        case "left"
            x = x + [0, -L];
            y = [y y];
        case "up"
            x = [x x];
            y = y + [0 +L];
        case "down"
            x = [x x];
            y = y + [0 -L];
    end

    % Set hold to true
    tf = ishold( ax );
    hold( ax, 'on' );

    % Draw waveguide
    line = plot( ax, x, y, 'Color', color, 'LineWidth', args.Width );
    % and tag
    Tags = precise.draw.tag( ax, mean(x), mean(y), num2str( i ) );

    % Reset hold to old position
    if tf
        hold( ax, 'on' );
    else
        hold( ax, 'off' );
    end
    Inputs = [];
    Outputs = [];
end
