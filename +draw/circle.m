function [Arcs, Inputs, Outputs, Tags ] = circle( ax, x, y, i, arcs, ...
    StartAngle, StopAngle, args )
%CIRCLE Summary of this function goes here
%   Detailed explanation goes here
    arguments
        ax
        x (1,1) double
        y (1,1) double
        i (1,1) double { mustBeInteger }
        arcs (1,1) double { mustBeInteger, mustBePositive } = 2
        StartAngle (1,1) double = -pi/2
        StopAngle (1,1) double = StartAngle + 2*pi;
        args.Color (:,:) = repmat( ['k';'y'], ceil( arcs/2 ), 1 )
        args.ClockWise logical = false
        args.Radius (1,1) double { mustBePositive } = 0.2
        args.Width (1,1) double { mustBePositive } = 5
    end

    guides = i+(1:arcs)-1;
    ends = linspace( StartAngle, StopAngle, arcs+1);
    theta = nan( length(ends)-1, 21 );
    for j = 1:length(ends)-1
        theta(j,:) = linspace( ends(j), ends(j+1), 21 );
    end

    if args.ClockWise
        theta = rot90( theta, 2 );
    end
    
    % Set hold to true
    tf = ishold( ax );
    hold( ax, 'on' );

    Arcs = gobjects( arcs, 1 );
    Tags = gobjects( arcs, 1 );

    % Draw arcs & tags
    for j = 1:size( theta, 1 )
        % Draw arcs
        Arcs(j,1) = plot( ax, ...
            x + args.Radius*cos( theta(j,:) ), ...
            y + args.Radius*sin( theta(j,:) ), ...
            'Color', args.Color(j,:), ...
            'LineWidth', args.Width ...
        );
        % add tags
        Tags(j,1) = precise.draw.tag( ax, ...
            x + args.Radius*cos( theta(j,11) ), ...
            y + args.Radius*sin( theta(j,11) ), ...
            num2str( guides(j) ) );
    end

    % Reset hold to old position
    if tf
        hold( ax, 'on' );
    else
        hold( ax, 'off' );
    end
    Inputs = [];
    Outputs = [];
end
