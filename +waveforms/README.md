# Waveform subpackage

This package contains functions used as waveforms for the amplitude of any
input signal.

The time scale should be considered in `ps`, whereas the amplitude scale
in `sqrt(mW)`.
