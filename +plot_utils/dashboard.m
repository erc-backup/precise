classdef dashboard < handle
    %DASHBOARD Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Title
        Figure
        Spectrum
        Scheme
        Combinations = struct( 'ax', [], 'xline', [] )
        Timeseries = struct( 'ax', [] )
        
        Iter_NL function_handle
        Iter_LIN function_handle
        Ind2J function_handle
    end
    
    methods
        function obj = dashboard(data, nonlinear_iter, linear_iter, ...
                ind2j, scheme, args)
            %DASHBOARD Construct an instance of this class
            %   Detailed explanation goes here
            arguments
                data
                nonlinear_iter function_handle
                linear_iter function_handle
                ind2j function_handle
                scheme function_handle
                args.Title = '';
            end
            
            if ~iscell(data)
                data = {data};
            end
            
            obj.Iter_NL  = nonlinear_iter;
            obj.Iter_LIN = linear_iter;
            obj.Ind2J = ind2j;
            
            obj.Figure = figure( 'Name', args.Title );
            
            % plot
            obj.draw_axes( scheme, data );
            
            % print subplot grid title
            obj.Title = sgtitle( args.Title );
            
            % update with first combination
            obj.update( 1 )
        end
        
        function draw_axes( obj, scheme, data )
            nActive = size(data{1},1)/2;
            N = size(data{1},2);
            colors = lines(nActive);
            
            % plot timeline-extracted values (end, rms, avg, min, max,
            % etc.) as function of parameter combination index j.
            % Temperature
            obj.Combinations(1) = struct( ...
                'ax', subplot( 4, 2, 5), ...
                'xline', xline( 0, '-.' ) ...
            );
            hold on
            if length(data) == 3
                edges = [data{2}, fliplr( data{3} )];
                for j = 1:nActive
                    patch( [1:N N:-1:1], edges(j,:), colors(j), ...
                        'EdgeColor', 'none', 'FaceAlpha', 0.1, ...
                        'HandleVisibility', 'off' ...
                    );
                end
            end
            for j = 1:nActive
                plot( data{1}(j,:), ['-', colors(j)] )
            end
            hold off
            ylabel( 'Temp. [K]' )
            % Free Carriers
            obj.Combinations(2) = struct( ...
                'ax', subplot( 4, 2, 7), ...
                'xline', xline( 0, '-.' ) ...
            );
            hold on
            if length(data) == 3
                for j = 1:nActive
                    patch( [1:N N:-1:1], edges(j+nActive,:), colors(j), ...
                        'EdgeColor', 'none', 'FaceAlpha', 0.1, ...
                        'HandleVisibility', 'off' ...
                    );
                end
            end
            for j = 1:nActive
                plot( data{1}(j+nActive,:), ['-', colors(j)] )
            end
            hold off
            ylabel( "F. C. [um^{-3}]")
            
            % linear spectrum
            obj.Spectrum = subplot( 4, 2, 1 );
            xlabel( 'Frequency [THz]' )
            ylabel( 'Energy Spectrum' )
            
            % system scheme
            obj.Scheme = subplot( 4, 2, 3);
            scheme( obj.Scheme, colors );
            
            %% Timeseries
            % Temperature
            obj.Timeseries(1).ax = subplot( 3, 2, 2);
            ytickformat("%.1f")
            ylabel( 'Temp. [K]' )
            xlabel( 'Time [ps]' );
            % Free Carriers
            obj.Timeseries(2).ax = subplot( 3, 2, 4);
            ytickformat("%.1f")
            ylabel( "F. C. [um^{-3}]")
            xlabel( 'Time [ps]' );
            % Fields
            obj.Timeseries(3).ax = subplot( 3, 2, 6);
            ytickformat("%.1f")
            ylabel( 'EM Energy' )
            xlabel( 'Time [ps]' );
        end
        
        function plot_timeseries( obj, time, DT, DN, opt)
            figure( obj.Figure );
            % Temperature
            subplot( obj.Timeseries(1).ax );
            set(gca,'NextPlot','replacechildren');
            plot( time, DT )
            % Free Carriers
            subplot( obj.Timeseries(2).ax );
            set(gca,'NextPlot','replacechildren');
            plot( time, DN )
            % Fields
            subplot( obj.Timeseries(3).ax );
            set(gca,'NextPlot','replacechildren');
            plot( time, opt )
        end
        
        function plot_spectrum( obj, f, opts )
            figure( obj.Figure );
            subplot( obj.Spectrum );
            set(gca,'NextPlot','replacechildren');
            plot( f, opts .* conj( opts ) )
        end
        
        function update( obj, j)
            if isnumeric( j ) && isscalar( j )
%                 j = j;
            elseif iscell( j )
                j = obj.Ind2J( j );
            else
                error( 'input must be either global index j or sub-indexes cell array.' )
            end
            
            [sol, time, fdeval] = obj.Iter_NL( j );
            [DT, DN, ~, opt] = fdeval( sol, time );
            
            obj.plot_timeseries( time, DT, DN, opt );
            
            obj.Combinations(1).xline.Value = j;
            obj.Combinations(2).xline.Value = j;
            
            % update spectrum
            [freq, opts] = obj.Iter_LIN( j );
            obj.plot_spectrum( freq, opts );
        end
    end
end
