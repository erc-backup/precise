function p = suppressing_linear_spectrum( f, o, Title, Legends, propArgs )
    arguments
        f (1,:) double { mustBePositive, mustBeReal }
        o (:,:) double { mustBeNonnegative, mustBeReal }
        Title (1,:) char
        Legends (1,:) cell
        propArgs.?matlab.graphics.chart.primitive.Line
    end
    propertyCell = namedargs2cell(propArgs);
    NameArray = propertyCell(1:2:end);
    ValueArray = cell( length(NameArray), size(o, 1) );
    for i=1:length(NameArray)
        ValueArray(i,:) = propertyCell{2*i};
    end
    
    %% suppress all zero outputs
    nonzero = sum(o, 2) ~= 0;
    nzo = o(nonzero, :);
    
    fprintf( '\nThe following fields have been suppressed because all zero:\n')
    if isempty(Legends)
        fprintf( '%s\n', strjoin( string( find( ~nonzero ) ), ', ') );
    else
        fprintf( '%s\n', strjoin( Legends( ~nonzero ), ', ') );
    end
    
    %% plot data
    if ~isempty( nzo )
        p = plot( f, nzo );
        set(p, NameArray, ValueArray.' );
        xlabel( 'THz' );
        if ~isempty(Title)
            title( Title );
        end
        if ~isempty( Legends )
            legend( Legends(  nonzero ) );
        else
            legend( );
        end
    else
        plot( f, zeros(size(f)) );
    end
end