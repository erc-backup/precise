function add_ring( obj, bus_guides, active )
%ADD_RING Add a ring module to the optical system
%   'bus_guides' indicates the number of bus waveguides connected to the
%   ring. When it is equal to 1 it produces an all pass microring, when it
%   is equal to 2 it produces an add drop microring. Higher values are
%   supported as well. The default value is 2 (add-drop configuration).
    arguments
        obj
        bus_guides { mustBeInteger, mustBeNonnegative } = 2
        active logical = true
    end

    % Get current port count
    p = obj.count.ports;

    % Add splitters
    for i = 1:bus_guides
        obj.add_splitter_2x2( );
    end

    % Add microring waveguides
    for i = 1:bus_guides-1
        obj.add_guide( p+i*4-1, p+i*4+4, active );
    end
    obj.add_guide( p+bus_guides*4-1, p+4, active );
    
end