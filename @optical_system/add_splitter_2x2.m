function add_splitter_2x2( obj, k )
%ADD_SPLITTER_2X2 Create a 2x2 splitter node.
%   Create a 4-port node with two sides, two ports per side. Each side is
%   linked to the other with the following matrix.
%   |right_above| = | t ik||left_above|
%   |right_below| = |ik  t||left_below|
%
%   The actual matrix containing the link coefficients is generated with
%   the `matrix_coeff_splitter_2x2` static method.
    arguments
        obj
        k = obj.get_new_coeff()
    end
    
    % get current port count
    p = obj.count.ports;
    
    % create a node with 2 input and 2 output ports, total of 4.
    obj.add_node( 4 );
    
    % generate the link matrix
    K = obj.matrix_coeff_splitter_2x2( k );
    
    % Add symmetric link
    obj.add_link( p+[1;2;3;4], p+[1;2;3;4], K );
end

