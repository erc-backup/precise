function add_scissor( obj, n, active, closed )
%ADD_SCISSOR Add a SCISSOR module to the optical system
%   Detailed explanation goes here
    arguments
        obj
        n { mustBeInteger, mustBePositive } = 2
        active { mustBeMember( active, {'rings only', 'full'} ) } = 'full'
        closed logical = false
    end
    
    % Get current port count
    p = obj.count.ports;
    
    % Popolate rings
    for i = 1:n
        obj.add_ring( 2 );
    end
    
    % Elaborate tracking flag
    switch active
        case 'rings only'
            active = false;
        case 'full'
            active = true;
    end
    
    % Add paths connecting one ring to another
    j = p;
    for i = 1:n-1
        j = p + 8*i;
        obj.add_guide( j-6, j+1, active );
        obj.add_guide( j+6, j-3, active );
    end
    
    % Add closing waveguide if requested.
    if closed
        obj.add_guide( j + 2, j + 5, active );
    end
    
end

