function add_termination( obj, port, active, k )
%ADD_TERMINATION Add a termination module to the optical system.
%   Detailed explanation goes here
    arguments
        obj
        port { mustBeInteger }
        active logical = true
        k = obj.get_new_coeff()
    end
    
    % get current port count
    p = obj.count.ports;
    
    % create a node with 2 input/output port.
    obj.add_node( 2 );
    
    % generate the link matrix
    K = obj.matrix_coeff_termination( k );
    
    % Add link
    obj.add_link( p+[1;2], p+[1;2], K );
    
    % Add waveguide
    obj.add_guide( port, p+1, active );
end
