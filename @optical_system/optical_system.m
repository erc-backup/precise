classdef optical_system < handle
%PRECISE.OPTICAL_SYSTEM Base class for creating complex modular optical structures.
%   Detailed explanation goes here

    properties
        count struct
        ports struct
        guides struct
    end
    
    methods
        function obj = optical_system( )
            %OPTICAL_SYSTEM Construct an instance of a generic optical
            %system
            %   Initialize the counter structure and the port and the
            %   guides structure which will contain the symbolic entities
            %   and their references.
            
            obj.count = struct('inputs', 0, 'ports', 0, 'guides', 0, ...
                'coeffs', 0);
            obj.ports = struct( 'i', [], 'o', [], 'ki', [], 'ko', [] );
            obj.guides = struct( 'ends', [], 'active', [] );
        end
        
        %% Base construction methods
        function add_node( obj, num_ports )
            %ADD_NODE Adds a node to the system
            %   This function adds a node with `num_ports` ports.
            %   A node is just a collection of ports; each port is
            %   considered both as input and output.
            %   
            %   For example a beam splitter (2x2) has 4 ports whereas a
            %   combiner (2x1) has 3 ports.
            arguments
                obj
                num_ports { mustBeInteger, mustBePositive }
            end
            
            n1 = obj.count.ports + 1;
            n2 = obj.count.ports + num_ports;
            
            in  = num2cell( sym( compose( 'i%d', n1:n2) ) );
            out = num2cell( sym( compose( 'o%d', n1:n2) ) );
            
            obj.ports(n1:n2) = struct( ...
                'i', in, 'o', out, ...
                'ki', 0, 'ko', 0 );
            
            obj.count.ports = n2;
            
        end
        function k = get_new_coeff( obj, n )
            %GET_NEW_COEFF Generates and retrieve new coefficients
            %   This function creates `n` new coefficients (by default 1),
            %   and gives them back as output.
            arguments
                obj
                n = 1
            end
            n1 = obj.count.coeffs + 1;
            nk = obj.count.coeffs + n;
            k = sym( compose( 'k%d', n1:nk) );
            obj.count.coeffs = nk;
        end
        function add_link( obj, a, b, K)
            %ADD_LINK Adds a link relation between ports of the same node.
            %   Adds a link relation (constant or coefficient) between the
            %   sets of ports p1 (scalar or vector) and p2 (scalar or
            %   vector). The dimensions of p1 and p2 should match the K
            %   matrix dimensions.
            %   
            %   |  |   |k1 k2 k3| |  |
            %   |ao| = |k4 k5 k6|*|bi|,
            %   |  |   |k7 k8 k9| |  |
            %   
            %   where the left vector contains output ports and the right
            %   vectors contains input ports. A port-to-port coefficient
            %   must be symmetric.
            arguments
                obj
                a (1,:) { mustBeInteger, mustBePositive }
                b (1,:) { mustBeInteger, mustBePositive }
                K (:,:) = []
            end
            L1 = length(a);
            L2 = length(b);
            
            if isempty( K )
                K = reshape( obj.get_new_coeff( L1*L2 ), [L2, L1] ).';
            else
                obj.mustMatchMatrixDims( K, L1, L2 )
            end
            
            A = [obj.ports( a ).ko].' + K * [obj.ports( b ).i].';
            
            for l = 1:L1
                obj.ports( a(l) ).ko = A( l );
            end
            
        end        
        function add_input( obj, p )
            %ADD_INPUT Define one port as global input.
            arguments
                obj
                p { mustBeInteger, mustBePositive }
            end
            mustBeLessThanOrEqual( p, obj.count.ports )
            
            obj.ports( p ).ki = obj.ports( p ).i;
            obj.count.inputs = obj.count.inputs + 1;
        end
        function add_guide( obj, p1, p2, active )
            arguments
                obj
                p1
                p2
                active logical = true
            end
            
            np = obj.count.ports;
            mustBeLessThanOrEqual( p1, np )
            mustBeLessThanOrEqual( p2, np )
            
            n = obj.count.guides + 1;
            
            obj.guides(n) = struct( 'ends', [p1, p2], 'active', active );
            
            ik0  = 2i*pi*sym('f')/sym('c0'); % imag. unit * wavevector in vacuum
            neff = sym( sprintf('neff%d', n) ); % effective index
            dn   = sym( sprintf('dn%d', n) ); % nonlinear index variation
            L    = sym( sprintf('L%d', n) ); % waveguide length
            
            phase = exp( ik0 .* (neff + dn) .* L );
            
            obj.ports(p2).ki = obj.ports(p2).ki + phase .* obj.ports(p1).o;
            obj.ports(p1).ki = obj.ports(p1).ki + phase .* obj.ports(p2).o;
            
            obj.count.guides = n;
            
        end
        
        %% Base symbolic evaluation
        function par_function = generate_pfe_function( obj )
            %GENERATE_FUNCTION Generates the function that produces the
            %matrix A and vector b in the system Ax = b.
            
            % Get counters
            p = find( [obj.ports.ki] == [obj.ports.i]);
            g = 1:obj.count.guides;
            c = 1:obj.count.coeffs;
            
            eqns = [obj.ports.o] == [obj.ports.ko];
            eqns = subs( eqns, [obj.ports.i], [obj.ports.ki]);
            
            % Select active/inactive guides
            if isempty( g )
                dn_active = sym( compose('dn%d', g) ).';
            else
                dn = sym( compose('dn%d', g) );
                dn_active   = nonzeros( dn .*  [obj.guides.active] );
                dn_inactive = nonzeros( dn .* ~[obj.guides.active] );
                % Put inactive guides' dn to 0
                eqns = subs( eqns, dn_inactive, zeros(size(dn_inactive)) );
            end

            % Get matricial form
            [A, b] = equationsToMatrix( eqns, [obj.ports.o] );
            
            vars = {sym('c0'), sym('f'), ...
                sym( compose('i%d', p) ).', ...
                sym( compose('k%d', c) ).', ...
                sym( compose('L%d', g) ).', ...
                sym( compose('neff%d', g) ).', ...
                dn_active ...
            };

            matrix_generator = matlabFunction( A, b, 'Vars', vars);
            
            function fields = par_field_eval( c0, f, in, k, L, neff, dn )
                % Values substitution
                [aa, bb] = matrix_generator( c0, f, in, k, L, neff, dn );

                % Solution via matrix inversion
                fields = linsolve( aa, bb );
            end
            
            par_function = @par_field_eval;
        end
        function [active, nActive] = get_active_elements( obj )
            
            % convert structure in table
            T = struct2table( obj.guides );
            
            % select only the active elements
            active = T.ends( T.active == true, : );
            if nargout == 2
                nActive = size(active, 1);
            end
        end
        
        %% custom structures methods
        add_splitter_2x2( obj, k )
        add_splitter_3x3( obj, k1, k2 )
        add_ring( obj )
        add_crow( obj, n )
        add_scissor( obj, n, closed )
        add_termination( obj, port, active, k )
        add_taiji_resonator( obj, active )
    end
    
    methods ( Static )
        M = matrix_coeff_splitter_2x2( symbol )
        M = matrix_coeff_splitter_3x3( symbol1, symbol2 )
        M = matrix_coeff_termination( symbol )
        function mustMatchMatrixDims( M, L1, L2 )
            S = size(M);
            if ~isequal( S, [L1, L2]  )
                error('Matrix and vectors dimensions must match.')
            end
        end
    end
end
