function M = matrix_coeff_termination( symbol )
% MATRIX_COEFF_TERMINATION Generates the coefficient matrix for a
% termination. It assumes the waveguides has a higher refractive index than
% the medium it terminates in.

    % Get short notation for transmission t and reflection k
    t = sqrt(1-symbol.^2);
    k = symbol;
    
    % Create matrix
    M = [ k  t ;
          t -k ];
end