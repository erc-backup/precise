function M = matrix_coeff_splitter_3x3( symbol1, symbol2 )
%MATRIX_COEFF_SPLITTER_3X3 Generate coefficient matrix for a 3x3 splitter
%with two symbolic coefficients 'symbol1' and 'symbol2'.

    % Get short notation for transmission t and reflection k
    t0 = sqrt( 1 - symbol2^2 - symbol1^2 );
    kk = symbol2.*symbol1 ./ ( 1 + t0 );
    ku = 1i * symbol2;
    kd = 1i * symbol1;
    tu = sqrt( 1 - kk.^2 - symbol2.^2 );
    td = sqrt( 1 - kk.^2 - symbol1.^2 );
    
    % Create matrix
    MUR = [ -kk  ku  tu ;
             kd  t0  ku ;
             td  kd -kk ];
    MDL = transpose( MUR );
 
    M = [ zeros(3), MUR ;
          MDL, zeros(3) ];
end
