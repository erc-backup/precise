# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-04-13

### Added
- Class for modular description of integrated optical systems `optical_system`, along with a number of methods for fundamental structures.
- `dataloader` function that enables the systematic generation of the combinations of system parameters
- `materials` subpackage containing class definitions of the material optical properties
- `waveforms` subpackage containing functions that describe the shape of the input signals
- `iterator` subpackage containing the linear and nonlinear iterators, along with the function that evaluate the nonlinear step solved by the ODE solver
- `analysis` subpackage containing a set of functions that extract figure-of-merits from the evaluated nonlinear response.
- `plot_utils` and `draw` subpackages providing some utility function to plot the output data and to draw the designed structures
- `examples` subpackage containing a set of useful linear and nonlinear examples
