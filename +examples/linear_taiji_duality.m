function [outcome] = linear_taiji_duality( argsin ) 
% LINEAR_TAIJI_DUALITY This function plots the linear spectrum of a TAIJI
% microresonator structure in the forward and backward configuration in
% order to compare them. All parameters are optional.
%
% Arguments:
%   - 'freq' (default 190:0.001:195): frequencies in [THz] at which the
%       spectrum is evaluated.
%   - 'ks' (default sqrt(0.1)): field coupling coefficients; can be scalar
%       or vector of length 2 or 7.
%   - 'lengths' (default [...]): guide segments lengths in [um]; can only
%       be a vector of length 8.
%   - 'neff' (default 2.4): effective refractive index.
%   - 'ng' (default 4): group index.
%   - 'loss'  (default 2e-4): is the loss value in [dB/um].
%   - 'area' (default 0.25*0.45): waveguide section area in [um^2].

    arguments
        argsin.freq (1,:) double = 192.1:0.0001:192.3
        argsin.ks (1,:) double = [0.4, 0.2]
        argsin.lengths (1,8) double = [208.5, 395.5, 208.5, 2*197, 5574, ...
            480, 124, 124];
        argsin.neff (1,1) double = 2.4;
        argsin.ng (1,1) double = 4;
        argsin.loss (1,1) double = 2e-4;
        argsin.area (1,1) double = 0.25*0.45;
    end
    %% single Taiji microresonator
    S = precise.optical_system();
    S.add_taiji_resonator( true );
    
    S.add_termination( 1, false )
    S.add_termination( 2, false )
    S.add_termination( 6, false )
    S.add_termination( 9, false )
    
    S.add_input( 14 );
    S.add_input( 16 );

    parfun = S.generate_pfe_function();
    [~, NA] = S.get_active_elements();
    NK = S.count.coeffs;
    NP = S.count.ports;
    
    %% adapt parameters to system
    % coupling coefficients
    if isscalar( argsin.ks )
        ks = repelem( argsin.ks, 1, NK );
    elseif length( argsin.ks ) == 2
        ks = repelem( argsin.ks, 1, 4 );
        ks(4) = [];
    elseif length( argsin.ks ) == NK
        ks = argsin.ks;
    else
        error( "'ks' must be either a scalar or a vector of length 2 or %d.", NK );
    end
    kcell = repmat( {3;nan;nan;1;"k coeff."}, 1, NK );
    kcell(2:3,:) = num2cell( [1:NK; ks] );
    % lengths
    lcell = repmat( {4;nan;nan;1;"Lengths"}, 1, 8 );
    lcell(2:3,:) = num2cell( [1:8; argsin.lengths] );
    
    %% create dataloader
    
    [loader, ~, ~, ~, ~] = precise.dataloader( ...
        1, 1:2,  0:1,         2:3, "Power", ...
        2, 1,    argsin.freq, 4,   "Frequency", ...
        kcell{:}, ...              "k coeff."
        lcell{:}, ...              "Lengths"
        5, 1:8, argsin.neff,  1,   "N_eff", ...
        6, 1:8, argsin.loss,  1,   "loss [dB/cm]", ...
        7, 1,    argsin.area, 1,   "Area", ...  % waveguide cross-section
        8, 1,    argsin.ng,   1,   "group index" ...
    );

    %% generate linear iterator
    LINfun = precise.iterator.linear( loader, parfun, argsin.freq, NP, NA );

    %% Evaluate data
    tic
    [f, o1] = LINfun( 2 );
    [~, o2] = LINfun( 3 );
    o = [ o1( [14, 16], :); o2( [16, 14], :) ];
    toc
    fprintf('...for %d points.\n', 2*length( f ) );

    %% defining legends
    Legends = {'Forward, Through', 'Forward, Backreflection', ...
        'Reverse, Through', 'Reverse, Backreflection'};
    Title = 'Taiji microring resonator, comparison of forward and backward configuration.';
    
    %% plotting figure
    figure();
    precise.plot_utils.suppressing_linear_spectrum( f, o .* conj(o), ...
        Title, Legends(:), 'LineStyle', {'-','-','--','--'}, ...
        'LineWidth', {1 1 2 2} );

    %% output
    if nargout
        outcome = {f, {o1, o2}, LINfun, loader, parfun, S, argsin};
    end
end
