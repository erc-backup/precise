%% NONLINEAR_SCISSOR
%   This script is provided as example in the solution of a scissor with
%   two microrings and one input signal.
%   The system is solved from 0 to 1000 [ns] every 100 [ps], for each
%   frequency around the resonance (191.878 THz) from -40 to +40 [GHz]
%   every 2 [GHz]. The coupling coefficients are all equal to 0.32. The
%   rings have radia of 5 um and are separated by 15*pi [um]. The waveguide
%   section is 0.25 * 0.45 um^2, the effective index is 2.4, the group
%   index is 4, and the linear loss is 2e-4 [dB/um].
%   
%   From the system solution, the average value and min/max values of the
%   timeseries are saved. Moreover, the period of the most important FFT
%   component is extracted for each timeserie.
%   
%   At the end of the analysis, several plots are shown and can be update
%   with the update( j ) method for each plot, requiring only the
%   combination index j as argument.
%   Example:
%       tic;
%       i = ind2j( {21, 13, 1, 1, 1} );
%       D.update( i ); V.update( i ); P.update( i );
%       toc
%   
%   This examples take ~15s loading the parpool and ~345s (5'45")
%   evaluating the system dynamic on an AMD R5 2500U laptop with 4 workers.

clear

%% build system
% number of rings
NR = 2;
% waveguide closing the through port to the add port of the system
closed = false;

S = precise.optical_system();
S.add_scissor( NR, 'full', closed );
S.add_input( 1 );

%% generate parametric field evaluator function
%   and get the active waveguides.

parfun = S.generate_pfe_function();
[active, NA] = S.get_active_elements();

%% generate loader function and number of combinations

powers = logspace(0.4,1.3,10);
freqs = 191.878 + 1e-3.*(-40:2:40);
Kcoeff = sqrt(0.05);
paths_ids = 2*NR+(1:2*NR-2+closed);
input_func = @(t) 1-exp(-t/10e3);

[loader, N, sources, j2ind, ind2j] = precise.dataloader( ...
    1, 1,           powers,     2, "Power", ...
    2, 1,           freqs,      1, "Frequency", ...
    3, 1:2*NR,      Kcoeff,     3, "k coeff.", ...
    4, 1:2*NR,      5*pi,       4, "Radia", ...
    4, paths_ids,   15*pi,      4, "Paths", ...
    5, 1:NA,        2.4,        5, "N_eff", ...
    6, 1:NA,        2e-4,       5, "loss [dB/cm]", ...
    7, 1,           0.25*0.45,  5, "Area", ...  % waveguide cross-section
    8, 1,           4,          5, "group index", ...
    9, 1,           45e-6,      5, "thermal constant", ...
    10, 1,          300e-6,     5, "free carrier constant" ...
);

%% ode solver additional arguments

time = 0:0.1e3:1000e3; % [ps]
initial_assumption = zeros( 2*NA, 1);
ode_options = odeset( ...
    'RelTol', 1e-6, ...
    ...'AbsTol', repelem( [0.1;1], nActive, 1), ...
    'NonNegative', 1:2*NA ...
);
NLfun = precise.iterator.nonlinear( loader, parfun, S.count.ports, active, ...
    input_func, precise.materials.silicon, time, initial_assumption, ...
    ode_options);

% linear iterator
LINfun = precise.iterator.linear( loader, parfun, freqs, S.count.ports, NA);

clear time initial_assumption ode_options

%% Preallocation of data arrays
Avg = nan(2*NA, N);
Max = nan(2*NA, N);
Min = nan(2*NA, N);
T   = nan(2*NA, N);

%% ode solver loop on all parameters combinations
if isempty( gcp('nocreate') )
    tic
    parpool;
    toc
    fprintf( "... time spent loading the parallel pool.\n\n" );
else
    fprintf( "Parallel pool already started.\n\n" );
end
fprintf( "Starting the data evaluation of %d combinations. (%s)\n", N, ...
    datestr(now, 'HH:MM:SS') );
tic
parfor j = 1:N
    
    [sol, t, fdeval] = feval( NLfun, j);
    
    % restrict analysis to second half of timeline
    [x, y] = precise.analysis.select_last_half( sol.x, sol.y );
    % select larger 2^N points in the second half of timeline
    t = precise.analysis.select_pow2_pts( t );
    % evaluate DT, DN, dn, and optical fields
    [DT, DN, ~, opts] = fdeval( sol, t );
    
    Avg(:,j) = trapz( x, y, 2) ./ ( x(end) - x(1) );
    Max(:,j) = max( y, [], 2);
    Min(:,j) = min( y, [], 2);
    
    T(:,j) = precise.analysis.periodFFT( t, [DN;DT], 0.05, false );

end
toc
fprintf( "...for %d combinations.\n\n", N);

%% Plot

D = precise.plot_utils.dashboard( {Avg, Min, Max}, NLfun, LINfun, ind2j, ...
    @( ax, col ) precise.draw.scissor( ax, 0, 0, 1, 1, NR, closed, col ), ...
    'Title', 'Dashboard' );

%% Visibility (DN and DT)
subtitles = compose( {'Temp., WG %d', 'F.C., WG %d'}, (1:NA)' );

V = precise.plot_utils.array_plot( ...
    (Max-Min)./(Max+Min), ... data
    sources, j2ind, 2, ... sources, index functions, and nr of rows
    'Subtitles', subtitles(:), ...
    'Title', "Visibility Contourf Plot" ...
);

clear subtitles

%% Period of the main FFT component (DN and DT)
subtitles = compose( {'Temp., WG %d', 'F.C., WG %d'}, (1:NA)' );

P = precise.plot_utils.array_plot( ...
    log10(T), ... data
    sources, j2ind, 2, ... sources, index functions, and nr of rows
    'Subtitles', subtitles(:), ...
    'Title', "Period Contourf Plot" ...
);

clear subtitles
