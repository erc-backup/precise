# Examples subpackage

This package provide a set of ready to use examples. Linear examples take
in consideration several structures and evaluate their linear spectrum.
Nonlinear examples analyze the nonlinear dynamic of a smaller number of
structures.