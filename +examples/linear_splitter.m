function [outcome] = linear_splitter( argsin )
% LINEAR_SPLITTER This function plots the linear spectrum of a 2x2
% splitter. All parameters are optional.
% 
% Arguments:
%   - 'freq', vector, frequencies in [THz] at which the spectrum is
%       evaluated.
%   - 'ks', double, field coupling coefficients.

    arguments
        argsin.freq (1,:) double = 190:0.01:195
        argsin.ks (1,1) double = sqrt(0.1)
    end
    %% 2x2 splitter
    S2x2 = precise.optical_system();
    S2x2.add_splitter_2x2();
    S2x2.add_input( 1 );
    
    parfun = S2x2.generate_pfe_function();
    [~, NA] = S2x2.get_active_elements();
    NP = S2x2.count.ports;
    
    %% create dataloader

    [loader, ~, ~, ~, ~] = precise.dataloader( ...
        1, 1,    1,           1, "Power", ...
        2, 1,    argsin.freq, 2, "Frequency", ...
        3, 1,    argsin.ks,   3, "k coeff.", ...
        7, 1,    0.25*0.48, 5, "Area", ...  % waveguide cross-section
        8, 1,    4,   5, "group index" ...
    );

    %% generate linear iterator
    LINfun = precise.iterator.linear( loader, parfun, argsin.freq, NP, NA );
    
    %% Evaluate data
    tic
    [f, o] = LINfun( 1 );
    toc
    fprintf('...for %d points.\n', length( f ) );
    
    %% defining legends
    Legends = {'in'; 'th'; 'add'; 'drop'};
    Title = 'Splitter 2x2';
    
    %% plotting figure
    figure();
    precise.plot_utils.suppressing_linear_spectrum( f, o .* conj(o), ...
        Title, Legends );

    %% output
    if nargout
        outcome = {f, o, LINfun, loader, parfun, S2x2, argsin};
    end
end
