function [outcome] = linear_scissor( argsin )
% LINEAR_SCISSOR This function plots the linear spectrum of a SCISSOR
% structure. All parameters are optional. By default the SCISSOR will have
% two rings: 'n' = 2.
%
% Arguments:
%   - 'n' (default 2): number of rings in the SCISSOR
%   - 'closed' (default false): loop-back waveguide connecting upper and
%       lower bus guides. 
%   - 'freq' (default 190:0.001:195): frequencies in [THz] at which the
%       spectrum is evaluated.
%   - 'ks' (default sqrt(0.1)): field coupling coefficients; can be scalar
%       or vector of length n or length 2n.
%   - 'radia' (default 7): radius value in [um]; can be a scalar or a
%       vector of length n or 2n.
%   - 'path' (default 7*pi): length of the bus guide segments between the
%       rings; can be a scalar or a vector of length 2n (2n+1 if closed is
%       true).
%   - 'neff' (default 2.4): effective refractive index.
%   - 'ng' (default 4): group index.
%   - 'loss'  (default 2e-4): is the loss value in [dB/um].
%   - 'area' (default 0.25*0.45): waveguide section area in [um^2].

    arguments
        argsin.n (1,1) double { mustBeInteger, mustBePositive } = 2;
        argsin.closed { mustBeMember( argsin.closed, 0:1 ) } = false
        argsin.freq (1,:) double = 190:0.001:195
        argsin.ks (1,:) double = sqrt(0.1)
        argsin.radia (1,:) double = 7;
        argsin.path (1,:) double = 7*pi;
        argsin.neff (1,1) double = 2.4;
        argsin.ng (1,1) double = 4;
        argsin.loss (1,1) double = 2e-4;
        argsin.area (1,1) double = 0.25*0.45;
    end
    %% scissor
    S = precise.optical_system();
    S.add_scissor( argsin.n, 'full', argsin.closed );
    S.add_input( 1 );

    parfun = S.generate_pfe_function();
    [~, NA] = S.get_active_elements();
    NK = S.count.coeffs;
    NL = 2*(argsin.n-1) + argsin.closed;
    NR = 2*argsin.n;
    NP = S.count.ports;
    
    %% adapt parameters to system
    % coupling coefficients
    if isscalar( argsin.ks )
        ks = repelem( argsin.ks, 1, NK );
    elseif length( argsin.ks ) == NK
        ks = argsin.ks;
    else
        error( "'ks' must be either a scalar or a vector of length %d.", NK );
    end
    kcell = repmat( {3;nan;nan;1;"k coeff."}, 1, NK );
    kcell(2:3,:) = num2cell( [1:NK; ks] );
    % radia
    if isscalar( argsin.radia )
        radia = pi .* repelem( argsin.radia, 1, NR );
    elseif length( argsin.radia ) == argsin.n
        radia = pi .* repelem( argsin.radia, 1, 2 );
    elseif length( argsin.radia ) == NR
        radia = pi .* argsin.radia;
    else
        error( "'radia' must be either a scalar or a vector of length %d or %d.", ...
            argsin.n, 2*argsin.n );
    end
    rcell = repmat( {4;nan;nan;1;"Radia"}, 1, NR );
    rcell(2:3,:) = num2cell( [1:NR; radia] );
    % paths
    if isscalar( argsin.path )
        paths = repelem( argsin.path, 1, NL );
    elseif length( argsin.path ) == NL
        paths = argsin.path;
    else
        error( "'path' must be either a scalar or a vector of length %d.", ...
            NL );
    end
    pcell = repmat( {4;nan;nan;1;"Paths"}, 1, NL );
    pcell(2:3,:) = num2cell( [NR+(1:NL); paths] );
    
    %% create dataloader
    
    [loader, ~, ~, ~, ~] = precise.dataloader( ...
        1, 1,    1,           1, "Power", ...
        2, 1,    argsin.freq, 2, "Frequency", ...
        kcell{:}, ...            "k coeff."
        rcell{:}, ...            "Radia"
        pcell{:}, ...            "Paths"
        5, 1:NA, argsin.neff, 1, "N_eff", ...
        6, 1:NA, argsin.loss, 1, "loss [dB/cm]", ...
        7, 1,    argsin.area, 1, "Area", ...  % waveguide cross-section
        8, 1,    argsin.ng,   1, "group index" ...
    );

    %% generate linear iterator
    LINfun = precise.iterator.linear( loader, parfun, argsin.freq, NP, NA );
    
    %% Evaluate data
    tic
    [f, o] = LINfun( 1 );
    toc
    fprintf('...for %d points.\n', length( f ) );
    
    %% defining legends
    Legends = compose( {'in%d'; 'th%d'; 'E%ddown+'; 'E%ddown-'; ...
        'add%d'; 'drop%d'; 'E%dup+'; 'E%dup-' }, 1:argsin.n);
    if argsin.closed
        Title = sprintf('SCISSOR with %d ring, closed', argsin.n);
    else
        Title = sprintf('SCISSOR with %d ring, open', argsin.n);
    end
    
    %% plotting figure
    figure();
    precise.plot_utils.suppressing_linear_spectrum( f, o .* conj(o), ...
        Title, Legends(:) );

    %% output
    if nargout
        outcome = {f, o, LINfun, loader, parfun, S, argsin};
    end
end
