# Materials package

The materials package contains a set of classes containing materials
properties.
Each class should provide the following constant properties:

- `density`, density in [g/um3]
- `spec_heat`, thermal capacity in [fJ/g/K]
- `th_cond`, thermal conductivity in [mW/um/K]
- `toe`, thermo-optic coefficient in [1/K]
- `bTPA`, Sigma_TPA coefficient in [um/mW]
- `sFCD`, real refractive index free carrier dependence in [um3]
- `sFCA`, imaginary refractive index free carrier dependence in [um2]
- `n0`, refractive index
- `n2`, intensity-dependent refractive index [um2/mW]

The materials package provides the following materials

- silicon
