classdef silicon
    %SILICON class for Silicon properties
    %   This class contains all the properties of Silicon
    
    properties( Constant = true )
        density     =  2.3290e-12    % [g/um3] density
        spec_heat   =  0.710e15      % [fJ/g/K] thermal capacity
        th_cond     =  0.149         % [mW/um/K] thermal conductivity
        toe         =  1.86e-4       % [1/K] thermo-optic coefficient
        bTPA        =  8.1e-9        % [um/mW] = 1e-3 [m/W] Sigma_TPA
        sFCD        = -4.0e-9        % [um3]=1e-18[m3] Sigma_FCD, real refractive index free carrier dependence
        sFCA        =  1.4e-9        % [um2]=1e-12[m2] Sigma_FCA, imaginary refractive index free carrier dependence
        n0          =  3.485         % [] refractive index
        n2          =  4.5e-9        % [um2/mW] = 1e-9 [m2/W] intensity-dependent refractive index
    end
end
